﻿using System;
using System.Collections.Generic;

namespace HDR.HierarchieProvider.Abstract
{
    public interface IHierarchieProvider
    {
        void        SetConfiguration(object config);
        void        ConnectToSource(Uri sourceUri);
        Dictionary<string,object> GetDataSourceAttributes();
        void        SetDataSourceAttributes(Dictionary<string,object> attributes);
        void        CreateSource(string sourceUri);
        string[]    GetDataGroupNamesAtPath(string pathName);
        void        SetDataGroupNamesAtPath(string pathName, string[] dataGroupNames);
        string[]    GetDataSetNamesAtPath(string pathName);
        void        SetDataSetAtPath(string pathName, string dataSetNames,int[] dimension, Type dataType);
        string[]    GetDataGroupAttributeNames(string pathName);
        void        SetDataGroupAttributeNames(string pathName, string[] attributeNames);
        string[]    GetDataSetAttributeNames(string pathName);
        void        SetDataSetAttributeNames(string pathName, string[] attributeNames );
        object      GetDataGroupAttributeValue(string pathName, string attributeName);
        void        SetDataGroupAttributeValue(string pathName, string attributeName,object attributeValue);
        object      GetDataSetAttributeValue(string pathName, string attributeName);
        void        SetDataSetAttributeValue(string pathName, string attributeName,object attributeValue);
        Array       GetDataSetArray(string pathName);
        void        SetDataSetArray(string pathName, Array array);
        void        DisConnectFromSource();
    }
}
