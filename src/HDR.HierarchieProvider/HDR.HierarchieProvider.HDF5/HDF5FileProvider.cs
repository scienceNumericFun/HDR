﻿using System;
using System.IO;
using HDR.HierarchieProvider.Abstract;
using AS.HDFql;
using System.Collections.Generic;
using System.Linq;

namespace HDR.HierarchieProvider.HDF5
{
    public class HDF5FileProvider : IHierarchieProvider
    {
        
        protected Uri _src;
        protected Dictionary<string,Dictionary<string,object>> _groupAttributes;
        protected Dictionary<string,Dictionary<string,object>> _setAttributes;
        
        protected string _CorrectPath(string pathName)
        {
            if (pathName.StartsWith("/"))
            {
                pathName = pathName.Substring(1);
            }
            else
            {
                // pass 
            }
            return pathName;
        }
        protected int _UseFile()
        {
            int errorCode ;
            string pwd = Directory.GetCurrentDirectory();
            FileInfo h5File = new FileInfo(_src.AbsolutePath);
            Directory.SetCurrentDirectory(h5File.Directory.ToString());
            
            string hdfqlCmd = "USE FILE " + h5File.Name;
            errorCode = HDFql.Execute(hdfqlCmd);

            Directory.SetCurrentDirectory(pwd);

            return errorCode;
        }
        public HDF5FileProvider()
        {
            _groupAttributes = new Dictionary<string, Dictionary<string, object>>();
            _setAttributes   = new Dictionary<string, Dictionary<string, object>>();
        }
        public void ConnectToSource(Uri sourceUri)
        {
            _src = sourceUri;
            
        }
        void IHierarchieProvider.DisConnectFromSource()
        {
            _src = null;
        }
        public void CreateSource(string sourceUri)
        {
            string pwd = Directory.GetCurrentDirectory();
            string dir = Path.GetDirectoryName(sourceUri);
            Directory.SetCurrentDirectory(dir);
            string fileName = Path.GetFileName(sourceUri);

            int errorCode ;   
            string hdfqlCmd = "CREATE FILE " + fileName;
            errorCode = HDFql.Execute(hdfqlCmd);

            Directory.SetCurrentDirectory(pwd);

            _src = new Uri(sourceUri);
        }
        public string[] GetDataGroupAttributeNames(string pathName)
        {
            int errorCode = _UseFile();
            if (_groupAttributes.Keys.Count == 0)
            {
                _groupAttributes[pathName] = HDFqlHelper.ShowAttributeInFile(pathName);    
            }
            else
            {
                // pass
            }
            
            return _groupAttributes[pathName].Keys.ToArray();
        }
        object IHierarchieProvider.GetDataGroupAttributeValue(string pathName, string attributeName)
        {
            HDFql.Execute("USE FILE " + _src.AbsolutePath);
            if (_groupAttributes.Keys.Count == 0)
            {
                _groupAttributes[pathName] = HDFqlHelper.ShowAttributeInFile(pathName);
            }
            else
            {
                // pass
            }
            return _groupAttributes[pathName][attributeName];
        }
        public string[] GetDataGroupNamesAtPath(string pathName)
        {
            int errorCode = _UseFile();

            pathName = _CorrectPath(pathName);

            string hdfqlCmd = "SHOW GROUP " + pathName;
            errorCode = HDFql.Execute(hdfqlCmd);
            
            string[] dataGroupNames = new string[(int) HDFql.CursorGetCount()];
            HDFql.CursorFirst();

            for(int idx =0; idx < dataGroupNames.Length;idx++)
            {
                dataGroupNames[idx] = HDFql.CursorGetChar();
                HDFql.CursorNext();
            }

            errorCode = HDFql.Execute("CLOSE FILE");

            return dataGroupNames;
        }
        public Array GetDataSetArray(string pathName)
        {
            Array array;
            int errorCode = _UseFile();
            
            pathName = _CorrectPath(pathName);

            errorCode = HDFql.Execute("SHOW DATATYPE " + pathName);

            HDFql.CursorFirst();

            int type = (int) HDFql.CursorGetInt();

            Type arrayElementType = HDFqlHelper.ConvertIntTypeToType(type);

            errorCode = HDFql.Execute("SHOW DIMENSION " + pathName);

            int[] dimension = new int[HDFql.CursorGetCount()];

            HDFql.CursorFirst();
            for(int idx = 0; idx < dimension.Length;idx++)
            {
                dimension[idx] = (int) HDFql.CursorGetInt();
                HDFql.CursorNext();
            }

            array = Array.CreateInstance(arrayElementType,dimension);

            string hdfqlCmd;
            HDFql.VariableRegister(array);

            hdfqlCmd = "SELECT FROM " + pathName + " INTO MEMORY 0";

            errorCode = HDFql.Execute(hdfqlCmd);

            HDFql.VariableUnregister(array);

            errorCode = HDFql.Execute("CLOSE FILE");
            
            return array;
        }
        string[] IHierarchieProvider.GetDataSetAttributeNames(string pathName)
        {
            HDFql.Execute("USE FILE " + _src.AbsolutePath);
            if (_setAttributes.Keys.Count == 0)
            {
                _setAttributes[pathName] = HDFqlHelper.ShowAttributeInFile(pathName);    
            }
            else
            {
                // pass
            }
            return _setAttributes[pathName].Keys.ToArray();
        }
        object IHierarchieProvider.GetDataSetAttributeValue(string pathName, string attributeName)
        {
            HDFql.Execute("USE FILE " + _src.AbsolutePath);
            if (_setAttributes.Keys.Count == 0)
            {
                _setAttributes[pathName] = HDFqlHelper.ShowAttributeInFile(pathName);    
            }
            else
            {
                // pass
            }
            return _setAttributes[pathName][attributeName];
        }
        public string[] GetDataSetNamesAtPath(string pathName)
        {
            int errorCode = _UseFile();

            pathName = _CorrectPath(pathName);

            string hdfqlCmd = "SHOW DATASET " + pathName;
            errorCode = HDFql.Execute(hdfqlCmd);
            
            string[] dataSetNames = new string[(int) HDFql.CursorGetCount()];
            HDFql.CursorFirst();

            for(int idx =0; idx < dataSetNames.Length;idx++)
            {
                dataSetNames[idx] = HDFql.CursorGetChar();
                HDFql.CursorNext();
            }

            errorCode = HDFql.Execute("CLOSE FILE");

            return dataSetNames;

        }

        Dictionary<string,object> IHierarchieProvider.GetDataSourceAttributes()
        {
            return null;
        }
        void IHierarchieProvider.SetConfiguration(object config)
        {

        }
        void IHierarchieProvider.SetDataGroupAttributeNames(string pathName, string[] attributeNames)
        {

        }
        void IHierarchieProvider.SetDataGroupAttributeValue(string pathName, string attributeName, object attributeValue)
        {

        }
        public void SetDataGroupNamesAtPath(string pathName, string[] dataGroupNames)
        {
            // e.g. debugging purpose
            int errorCode = _UseFile();

            pathName = _CorrectPath(pathName);

            string hdfqlCmd;
     
            foreach(var dataGroupName in dataGroupNames)
            {
                if (pathName.Equals(""))
                {
                    hdfqlCmd = "CREATE GROUP " + dataGroupName ;
                    errorCode = HDFql.Execute(hdfqlCmd);
                }
                else
                {
                    hdfqlCmd = "CREATE GROUP " + pathName + "/" + dataGroupName ;
                    errorCode = HDFql.Execute(hdfqlCmd);
                }
            }
            errorCode = HDFql.Execute("CLOSE FILE");
        }
        public void SetDataSetArray(string pathName, Array array)
        {
             // e.g. debugging purpose
            int errorCode = _UseFile();
            
            pathName = _CorrectPath(pathName);

            string hdfqlCmd;
            HDFql.VariableRegister(array);

            hdfqlCmd = "INSERT INTO " + pathName + " VALUES FROM MEMORY 0";

            errorCode = HDFql.Execute(hdfqlCmd);

            HDFql.VariableUnregister(array);

            errorCode = HDFql.Execute("CLOSE FILE");

        }
        void IHierarchieProvider.SetDataSetAttributeNames(string pathName, string[] attributeNames)
        {

        }
        void IHierarchieProvider.SetDataSetAttributeValue(string pathName, string attributeName, object attributeValue)
        {

        }
        public void SetDataSetAtPath(string pathName, string dataSetName,int[] dimension, Type dataType)
        {
            // e.g. debugging purpose
            int errorCode = _UseFile();

            pathName = _CorrectPath(pathName);

            string hdfqlCmd;

            string dim = "(";

            for (int idx = 0; idx < dimension.Length-1;idx++)
            {
                dim = dim + dimension[idx] + ",";
            }
            
            dim = dim + dimension.Last().ToString() + ")";
     
            if (pathName.Equals(""))
            {
                hdfqlCmd = "CREATE DATASET " + dataSetName + " AS "
                                    + HDFqlHelper.ConvertTypeToHDFqlType(dataType) + dim;
                errorCode = HDFql.Execute(hdfqlCmd);
            }
            else
            {
                hdfqlCmd = "CREATE DATASET " + pathName + "/" + dataSetName + " AS "
                                    + HDFqlHelper.ConvertTypeToHDFqlType(dataType) + dim;
                errorCode = HDFql.Execute(hdfqlCmd);
            }
            
            errorCode = HDFql.Execute("CLOSE FILE");
        }
        void IHierarchieProvider.SetDataSourceAttributes(Dictionary<string, object> attributes)
        {

        }
        
    }
}
