using System;
using AS.HDFql;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace HDR.HierarchieProvider.HDF5
{
    enum HDF5Element
    {
        ATTRIBUTE,
        GROUP,
        DATASET
    }
    internal static class HDFqlHelper
    {
        internal static bool TestHDFFile(Uri path)
        {
            bool fileExist = false;

            fileExist = File.Exists(path.AbsolutePath);

            int valid = HDFql.Execute("USE FILE \"" + path.AbsolutePath + "\"");

            fileExist = (valid == 0) && fileExist;

            return fileExist;
        }
        internal static Dictionary<string,object> ShowAttributeInFile(string hdfPath )
        {
            var attributes = new System.Collections.Generic.Dictionary<string,object>();

            HDFql.Execute("SHOW ATTRIBUTE " + hdfPath);

            int elementCardinality = HDFql.CursorGetCount();
            HDFql.CursorFirst();

            string[] attNames = new string[elementCardinality];

            for (int idx = 0; idx < elementCardinality;idx++ )
            {
                attNames[idx] = HDFql.CursorGetChar();

                HDFql.CursorNext();
            }

            for (int idx = 0; idx < elementCardinality;idx++)
            {
                HDFql.Execute("SHOW DATATYPE " + hdfPath + "/" + attNames[idx]);
                HDFql.CursorFirst();
                int datatype = (int) HDFql.CursorGetInt();

                HDFql.Execute("SELECT FROM " + hdfPath + "/" + attNames[idx] );

                var attributeValues = new object[(int)HDFql.CursorGetInt()];

                SetAttributeArray(datatype,attributeValues);

                if (attributeValues.Length == 1)
                {
                    attributes[attNames[idx]] = attributeValues[0];
                }
                else
                {
                    attributes[attNames[idx]] = attributeValues;
                }
            }
            
            return attributes;
        }
        internal static Type ConvertIntTypeToType(int datatype)
        {
            Type type = null;
            if (datatype == HDFql.TinyInt || datatype == HDFql.VarTinyInt)
            {
                type = typeof(sbyte);
            }
            else if (datatype == HDFql.UnsignedTinyInt || datatype == HDFql.UnsignedVarTinyInt)
            {
                type = typeof(byte);
            }
            else if (datatype == HDFql.SmallInt || datatype == HDFql.VarSmallInt)
            {
                type = typeof(short);
            }
            else if (datatype == HDFql.UnsignedSmallInt || datatype == HDFql.UnsignedVarSmallInt)
            {
                type = typeof(ushort);
            }
            else if (datatype == HDFql.Int || datatype == HDFql.VarInt)
            {
                type = typeof(int);
            }
            else if (datatype == HDFql.UnsignedInt || datatype == HDFql.UnsignedVarInt)
            {
                type = typeof(uint);
            }
            else if (datatype == HDFql.BigInt || datatype == HDFql.VarBigInt)
            {
                type = typeof(long);
            }
            else if (datatype == HDFql.UnsignedBigInt || datatype == HDFql.UnsignedVarBigInt)
            {
                type = typeof(ulong);
            }
            else if (datatype == HDFql.Float || datatype == HDFql.VarFloat)
            {
                type = typeof(float);
            }
            else if (datatype == HDFql.Double || datatype == HDFql.VarDouble)
            {
                type = typeof(double);
            }
            else if (datatype == HDFql.Char || datatype == HDFql.VarChar)
            {
                type = typeof(string);
            }
            return type;
        }
        internal static string ConvertTypeToHDFqlType(Type dataType)
        {
            string hdfqlName = ""; 
            if (dataType == typeof(sbyte))
            {
                hdfqlName = "TINYINT";
            }
            else if(dataType == typeof(byte))
            {
                hdfqlName = "UNSIGNED TINYINT";
            }
            else if (dataType == typeof(short))
            {
                hdfqlName = "SMALLINT";
            }
            else if (dataType == typeof(ushort))
            {
                hdfqlName = "UNSIGNED SMALLINT";
            }
            else if (dataType == typeof(int))
            {
                hdfqlName = "INT";
            }
            else if (dataType == typeof(uint))
            {
                hdfqlName = "UNSIGNED INT";
            }
            else if (dataType == typeof(long))
            {
                hdfqlName = "BIGINT";
            }
            else if (dataType == typeof(ulong))
            {
                hdfqlName = "UNSIGNED BIGINT";
            }
            else if (dataType == typeof(float))
            {
                hdfqlName = "FLOAT";
            }
            else if (dataType == typeof(double))
            {
                hdfqlName = "DOUBLE";
            }
            else if (dataType == typeof(string))
            {
                hdfqlName = "CHAR";
            }
            else
            {

            }
            return hdfqlName;
        }
        internal static void SetAttributeArray(int datatype, object[] array)
        {
            if (datatype == HDFql.TinyInt || datatype == HDFql.VarTinyInt)
            {
                for (int idx = 0; idx < array.Length;idx++)
                {
                    array[idx] = (sbyte) HDFql.CursorGetTinyInt();
                    HDFql.CursorNext();
                }
            }
            else if (datatype == HDFql.UnsignedTinyInt || datatype == HDFql.UnsignedVarTinyInt)
            {
                for (int idx = 0; idx < array.Length;idx++)
                {
                    array[idx] = (byte) HDFql.CursorGetUnsignedTinyInt();
                    HDFql.CursorNext();
                }
            }
            else if (datatype == HDFql.SmallInt || datatype == HDFql.VarSmallInt)
            {
                for (int idx = 0; idx < array.Length;idx++)
                {
                    array[idx] = (short) HDFql.CursorGetSmallInt();
                    HDFql.CursorNext();
                }
            }
            else if (datatype == HDFql.UnsignedSmallInt || datatype == HDFql.UnsignedVarSmallInt)
            {
                for (int idx = 0; idx < array.Length;idx++)
                {
                    array[idx] = (ushort) HDFql.CursorGetUnsignedSmallInt();
                    HDFql.CursorNext();
                }
            }
            else if (datatype == HDFql.Int || datatype == HDFql.VarInt)
            {
                for (int idx = 0; idx < array.Length;idx++)
                {
                    array[idx] = (int) HDFql.CursorGetInt();
                    HDFql.CursorNext();
                }
            }
            else if (datatype == HDFql.UnsignedInt || datatype == HDFql.UnsignedVarInt)
            {
                for (int idx = 0; idx < array.Length;idx++)
                {
                    array[idx] = (uint) HDFql.CursorGetUnsignedInt();
                    HDFql.CursorNext();
                }
            }
            else if (datatype == HDFql.BigInt || datatype == HDFql.VarBigInt)
            {
                for (int idx = 0; idx < array.Length;idx++)
                {
                    array[idx] = (long) HDFql.CursorGetBigInt();
                    HDFql.CursorNext();
                }
            }
            else if (datatype == HDFql.UnsignedBigInt || datatype == HDFql.UnsignedVarBigInt)
            {
                for (int idx = 0; idx < array.Length;idx++)
                {
                    array[idx] = (ulong) HDFql.CursorGetUnsignedBigInt();
                    HDFql.CursorNext();
                }
            }
            else if (datatype == HDFql.Float || datatype == HDFql.VarFloat)
            {
                for (int idx = 0; idx < array.Length;idx++)
                {
                    array[idx] = (float) HDFql.CursorGetFloat();
                    HDFql.CursorNext();
                }
            }
            else if (datatype == HDFql.Double || datatype == HDFql.VarDouble)
            {
                for (int idx = 0; idx < array.Length;idx++)
                {
                    array[idx] = (double) HDFql.CursorGetDouble();
                    HDFql.CursorNext();
                }
            }
            else if (datatype == HDFql.Char || datatype == HDFql.VarChar)
            {
                for (int idx = 0; idx < array.Length;idx++)
                {
                    array[idx] = (string) HDFql.CursorGetChar();
                    HDFql.CursorNext();
                }
            }
        }
        internal static void CreateAttributeToDataElement(string hdfPath,string attributeName, Type type, int[] dimension)
        {
            string hdfqlType = ConvertTypeToHDFqlType(type);

            string dimString = "(";
           
            for (int idx = 0; idx < dimension.Length-1;idx++)
            {
                dimString = dimString + dimension[idx] + ",";
            }
            dimString = dimString + dimension.Last() + ")";

            
        }
    }
}