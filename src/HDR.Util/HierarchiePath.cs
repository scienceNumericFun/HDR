﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HDR.Util
{
    public static class HierarchiePath
    {
        public static string GetParent(string path)
        {
            string parent = "";
            if (path.Equals("/"))
            {
                parent = null;
            }
            else
            {
                int pos = path.LastIndexOf('/');
                parent = path.Substring(0,pos-1);
            }
            return parent;
        }
    }
}
