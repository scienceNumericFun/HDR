using System.Collections.Generic;
using System.Linq;

namespace HDR.Core
{
    public class DataGroup : IDataGroup
    {
        protected string _absPath;
        protected IDataGroup _parent;
        protected IDataSource _src;
        protected Dictionary<string,object> _attributes;
        protected IDataGroup[] _dataGroups;
        protected IDataSet[] _dataSets;
        public string AbsPath
        {
            get
            {
                return _absPath;
            }
            set
            {
                if (!value.StartsWith("/"))
                {
                    value = "/" + value;
                }
                else
                {

                }
                if (!value.EndsWith("/"))
                {
                    value = value + "/";
                }
                else 
                {

                }
                _absPath = value;
            }
        }
        public string DataGroupName 
        {
            get
            {
                string groupName = null;
                if (_absPath == "/")
                {
                    groupName = "/";
                }
                else 
                {
                    var groups = _absPath.Split('/');
                    groupName = groups[groups.Length-2];
                }
                return groupName;
            }
        }
        public IDataGroup Parent
        {
            get
            {
                if (_parent == null)
                {
                    string parentPath = HDR.Util.HierarchiePath.GetParent(_absPath);
                    _parent = new DataGroup();
                    _parent.AbsPath = parentPath;
                    _parent.ConnectWithDataSourceObj(_src);
                }
                else
                {
                    // pass
                }
                return _parent;
            }
            set
            {
                _parent = value;
            }
        }
        public Dictionary<string,object> Attributes
        {
            get
            {
                if (_attributes == null)
                {
                    string[] names = _src.Provider.GetDataGroupAttributeNames(_absPath);
                    var attributes = new Dictionary<string,object>();

                    foreach(var name in names)
                    {
                        attributes[name] = _src.Provider.GetDataGroupAttributeValue(_absPath,name);
                    }
                    _attributes = attributes;
                }
                else
                {
                    // pass
                }
                return _attributes;
            }
            set
            {
                _attributes = value;
            }
        }
        public IDataGroup[] DataGroups
        {
            get
            {
                if (_dataGroups == null)
                {
                    string[] groupNames = _src.Provider.GetDataGroupNamesAtPath(_absPath);
                    if (groupNames != null)
                    {
                        _dataGroups = new DataGroup[groupNames.Length];
                        for(int idx = 0; idx < _dataGroups.Length; idx++)
                        {
                            _dataGroups[idx] = new DataGroup();
                            _dataGroups[idx].AbsPath = _absPath + groupNames[idx];
                            _dataGroups[idx].ConnectWithDataSourceObj(_src);
                        }
                    }
                    else
                    {
                        // pass
                    }
                }
                else
                {
                    // pass
                }
                return _dataGroups;
            }
            set
            {
                _dataGroups = value;
            }
        }
        public IDataSet[] DataSets
        {
            get
            {
                if(_dataSets == null)
                {
                    string[] setNames = _src.Provider.GetDataSetNamesAtPath(_absPath);
                    if (setNames != null)
                    {
                        _dataSets = new DataSet[setNames.Length];
                        for(int idx = 0; idx < _dataSets.Length; idx++)
                        {
                            _dataSets[idx] = new DataSet();
                            _dataSets[idx].AbsPath = _absPath + setNames[idx];
                            _dataSets[idx].ConnectWithDataSourceObj(_src);
                        }
                    }
                    else
                    {
                        // pass
                    }
                }
                else
                {
                    // pass
                }
                return _dataSets;
            }
            set
            {
                _dataSets = value;
            }
        }
        public void ConnectWithDataSourceObj(IDataSource src)
        {
            _src = src;
        }
        public void DisConnectFromDataSourceObj()
        {
            _src = null;
        }
    }
}