﻿using System;
using HDR.HierarchieProvider.Abstract;
using System.Collections.Generic;

namespace HDR.Core
{
    public interface IDataSource
    {
        void        ConnectToSource<T>(Uri source) where T : IHierarchieProvider;
        void        CreateSource<T>(string source) where T : IHierarchieProvider;
        IDataGroup  DataGroup {get;set;}
        void        DisConnectFromSource();
        Dictionary<string,object>   Attributes {get;set;}
        IHierarchieProvider         Provider {get;}
        Uri         Source {get;}
        void        Save();
    }
}
