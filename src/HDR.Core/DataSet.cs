using System;
using System.Collections.Generic;
using System.Linq;

namespace HDR.Core
{
    public class DataSet : IDataSet
    {
        protected IDataSource _dataSrc;
        protected string _absPath;
        protected Dictionary<string,object> _attributes;
        protected IDataGroup _parent;
        protected Array _array;
        public string AbsPath
        {
            get
            {
                return _absPath;
            }
            set
            {
                _absPath = value;
            }
        }
        public Dictionary<string,object> Attributes
        {
            get
            {
                if (_attributes == null)
                {
                    string[] names = _dataSrc.Provider.GetDataSetAttributeNames(_absPath);
                    var attributes = new Dictionary<string,object>();

                    foreach(var name in names )
                    {
                        attributes[name] = _dataSrc.Provider.GetDataSetAttributeValue(_absPath,name);
                    }
                    _attributes = attributes;
                }
                else
                {
                    // pass
                }
                return _attributes;
            }
            set
            {
                _attributes = value;
            }
        }
        public IDataGroup Parent
        {
            get
            {
                if (_parent == null)
                {
                    string parentPath = HDR.Util.HierarchiePath.GetParent(_absPath);
                    // just check valid name
                    _dataSrc.Provider.GetDataGroupAttributeNames(parentPath);
                    _parent = new DataGroup();
                    _parent.AbsPath = parentPath;
                    _parent.ConnectWithDataSourceObj(_dataSrc);
                }
                else
                {
                    // pass
                }
                return _parent;
            }
            set
            {
                _parent = value;
            }
        }
        public Array DataArray
        {
            get
            {
                if (_array == null )
                {
                    _array = _dataSrc.Provider.GetDataSetArray(_absPath);
                }
                else
                {
                    // pass
                }
                return _array;
            }
            set
            {
                _array = value;
            }
        }
        public void ConnectWithDataSourceObj(IDataSource src)
        {
            _dataSrc = src;
        }
        public void DisConnectFromDataSourceObj()
        {
            _dataSrc = null;
        }
    }
}