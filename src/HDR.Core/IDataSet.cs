using System.Collections.Generic;
using System;

namespace HDR.Core
{
    public interface IDataSet
    {
        string          AbsPath {get;set;}
        IDataGroup      Parent {get;set;}
        Dictionary<string,object> Attributes {get;set;}
        Array           DataArray {get;set;}
        void            ConnectWithDataSourceObj(IDataSource src);
        void            DisConnectFromDataSourceObj();
    }
}