using System;
using System.Collections.Generic;
using HDR.HierarchieProvider.Abstract;

namespace HDR.Core
{
    public class DataSource : IDataSource
    {
        protected IDataGroup _dataGroup;
        protected Uri _srcUri;
        protected Dictionary<string,object> _attributes;
        protected HDR.HierarchieProvider.Abstract.IHierarchieProvider _provider;
        public IDataGroup DataGroup
        {
            get
            {
                if(_dataGroup == null)
                {
                    _dataGroup = new DataGroup();
                    _dataGroup.AbsPath = "/";
                    _dataGroup.ConnectWithDataSourceObj(this);
                }
                else
                {
                    // pass
                }
                return _dataGroup;
            }
            set
            {
                _dataGroup = value;
            }
        }
        public Uri Source
        {
            get
            {
                return _srcUri;
            }
        }
        public Dictionary<string,object> Attributes
        {
            get
            {
                if (_attributes == null)
                {
                    _attributes = _provider.GetDataSourceAttributes();
                }
                else
                {
                    //
                }
                return _attributes;
            }
            set
            {
                _attributes = value;
            }
        }
        public void CreateSource<T>(string source) where T : IHierarchieProvider
        {
            _provider = (HierarchieProvider.Abstract.IHierarchieProvider) Activator.CreateInstance(typeof(T));
            _provider.CreateSource(source);
            _srcUri = new Uri(source);
        }
        public void ConnectToSource<T>(System.Uri source) where T : IHierarchieProvider
        {
            _srcUri = source;
            _provider = (HierarchieProvider.Abstract.IHierarchieProvider) Activator.CreateInstance(typeof(T));
            _provider.ConnectToSource(_srcUri);
        }
        public void DisConnectFromSource()
        {
            _provider.DisConnectFromSource();
            _provider = null;
            _srcUri = null;
        }
        HDR.HierarchieProvider.Abstract.IHierarchieProvider IDataSource.Provider
        {
            get
            {
                return _provider;
            }
        }
        void IDataSource.Save()
        {
        }
        public static DataSource CreateSrcObjFromSrc(string src, IHierarchieProvider provider)
        {
            var srcObj = new DataSource();
            srcObj._srcUri = new Uri(src);
            srcObj._provider = (HierarchieProvider.Abstract.IHierarchieProvider) Activator.CreateInstance(provider.GetType());
            srcObj._provider.ConnectToSource(srcObj._srcUri);

            return srcObj;
        }
    }
}