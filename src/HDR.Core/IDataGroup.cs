using System;
using System.Collections.Generic;

namespace HDR.Core
{
    public interface IDataGroup
    {
        string          AbsPath {get;set;}
        Dictionary<string,object> Attributes {get;set;}
        IDataGroup[]    DataGroups {get;set;}
        IDataSet[]      DataSets {get;set;}
        IDataGroup      Parent {get;set;}
        void ConnectWithDataSourceObj(IDataSource src);
        void DisConnectFromDataSourceObj();
    }
}