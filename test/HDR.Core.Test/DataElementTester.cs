using System;
using Xunit;
using HDR.HierarchieProvider.HDF5;
using System.IO;

namespace HDR.Core.Test
{
    public class DataElementTester : IDisposable
    {
        protected string _HDF5File;
        protected DataSource _dataSrc;
        protected HDF5FileProvider _provider;

         protected void _CreateHierarchy()
        {
            _provider.SetDataGroupNamesAtPath("/",new string[] {"g1","g2"} );
            _provider.SetDataGroupNamesAtPath("/g1",new string[]{"g3"});
            _provider.SetDataSetAtPath("/g2","s1",new int[] {2,2}, typeof(double));
            _provider.SetDataSetAtPath("/g1/g3","s2", new int[]{1,10}, typeof(float));
        }
        protected void _SetDataSets()
        {
            float[] s2 = new float[10];
            for (int idx = 0; idx < 10; idx++)
            {
                s2[idx] = (float)idx + 0.5f;
            }
            double[,] s1 = new double[2,2];

            s1[0,0] = 9.25;
            s1[1,0] = 7.75;
            s1[0,1] = 5.2543;
            s1[1,1] = 10.25;

            _provider.SetDataSetArray("/g1/g3/s2",s2);
            _provider.SetDataSetArray("/g2/s1",s1);
        }

        public DataElementTester()
        {
            _dataSrc = new DataSource();
            _provider = new HDF5FileProvider();
      
            _HDF5File = Directory.GetCurrentDirectory() +  Path.DirectorySeparatorChar +"DataElementTester.h5";

            _provider.CreateSource(_HDF5File);
            _CreateHierarchy();
            _SetDataSets();

            _dataSrc.ConnectToSource<HDF5FileProvider>(new Uri(_HDF5File));

            var root = _dataSrc.DataGroup;

            var groups = root.DataGroups;

            var g3 = groups[0].DataGroups[0];

            var s2 = g3.DataSets; 
        }
        public void Dispose()
        {
            File.Delete(_HDF5File);
        }
        [Fact]
        public void Test1()
        {

        }
    }
}
