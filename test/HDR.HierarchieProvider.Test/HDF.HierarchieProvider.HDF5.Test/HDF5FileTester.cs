using System.IO;
using System;
using Xunit;
using HDR.HierarchieProvider.HDF5;

namespace HDF.HierarchieProvider.HDF5.Test
{
    public class HDF5FileTester : IDisposable
    {
        protected HDF5FileProvider _provider;
        protected string _testFileName;
        protected void _CreateHierarchy()
        {
            _provider.SetDataGroupNamesAtPath("/",new string[] {"g1","g2"} );
            _provider.SetDataGroupNamesAtPath("/g1",new string[]{"g3"});
            _provider.SetDataSetAtPath("/g2","s1",new int[] {2,2}, typeof(double));
            _provider.SetDataSetAtPath("/g1/g3","s2", new int[]{1,10}, typeof(float));
        }
        protected void _SetDataSets()
        {
            float[] s2 = new float[10];
            for (int idx = 0; idx < 10; idx++)
            {
                s2[idx] = (float)idx + 0.5f;
            }
            double[,] s1 = new double[2,2];

            s1[0,0] = 9.25;
            s1[1,0] = 7.75;
            s1[0,1] = 5.2543;
            s1[1,1] = 10.25;

            _provider.SetDataSetArray("/g1/g3/s2",s2);
            _provider.SetDataSetArray("/g2/s1",s1);
        }
        public HDF5FileTester()
        {
            _provider = new HDF5FileProvider();
            _testFileName = Directory.GetCurrentDirectory() +  Path.DirectorySeparatorChar +"TestFile.h5";

            _provider.CreateSource(_testFileName);
            _CreateHierarchy();
            _SetDataSets();
        }
        public void Dispose()
        {
            File.Delete(_testFileName);
        }
        [Fact]
        public void CheckCreation()
        {
            Assert.True(File.Exists(_testFileName));
        }
        [Fact]
        public void CheckHierachie()
        {
            var groups = _provider.GetDataGroupNamesAtPath("/");
            
            Assert.NotNull(groups);
            Assert.NotEmpty(groups);
            Assert.Equal(groups.Length,2);
            Assert.Equal(groups[0],"g1");
            Assert.Equal(groups[1],"g2");

            groups = _provider.GetDataGroupNamesAtPath("/g1/");

            Assert.NotNull(groups);
            Assert.NotEmpty(groups);
            Assert.Equal(groups.Length,1);
            Assert.Equal(groups[0],"g3");

            groups = _provider.GetDataGroupNamesAtPath("/g2/");

            Assert.NotNull(groups);
            Assert.Empty(groups);

            string[] sets = _provider.GetDataSetNamesAtPath("/g1/g3/");

            Assert.NotNull(sets);
            Assert.NotEmpty(sets);
            Assert.Equal(sets.Length,1);
            Assert.Equal(sets[0],"s2");

            sets = _provider.GetDataSetNamesAtPath("/g2/");

            Assert.NotNull(sets);
            Assert.NotEmpty(sets);
            Assert.Equal(sets.Length,1);
            Assert.Equal(sets[0],"s1");
        }
        [Fact]
        public void CheckArrays()
        {
            var s1 = _provider.GetDataSetArray("/g2/s1");
            Assert.NotNull(s1);
            Assert.NotEmpty(s1);
            

            var s2 = _provider.GetDataSetArray("/g1/g3/s2");
            Assert.NotNull(s2);
            Assert.NotEmpty(s2);
            Assert.Equal(s2.Length,10);
            
            var s2Enum = s2.GetEnumerator();
            int idx = 0;

            while(s2Enum.MoveNext())
            {
                Assert.Equal(s2Enum.Current,idx+0.5f);
                idx++;
            }

        }
    }
}
