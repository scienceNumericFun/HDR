# HDR - Hierarchical Data Representation 

HDR is an experimental library for access data sources like SQL, CSV and HDF5 and represent them in a hierarchical form. 
It is inspired by the awesome HDF5 format and shall give many languages the opportunity to have a standardized access method and 
representation in memory (since many great languages have libraries to access .NET DLLs).


