New-Item -Path ./temp -ItemType Directory;

Invoke-WebRequest -Uri "http://www.hdfql.com/releases/1.5.0/HDFql-1.5.0_Linux64_GCC-4.9.zip" -OutFile .\temp\HDFQL_linux.zip;
Invoke-WebRequest -Uri "http://www.hdfql.com/releases/1.5.0/HDFql-1.5.0_Windows64_VS-2015.zip" -OutFile .\temp\HDFQL_windows.zip;

Add-Type -AssemblyName System.IO.Compression.FileSystem;

[System.IO.Compression.ZipFile]::ExtractToDirectory('.\temp\HDFQL_linux.zip', '.\temp\HDFQL_linux');
[System.IO.Compression.ZipFile]::ExtractToDirectory('.\temp\HDFQL_windows.zip', '.\temp\HDFQL_windows');

Copy-Item   -Path .\temp\HDFQL_linux\hdfql-1.5.0\wrapper\csharp\libHDFql.so `
            -Destination .\test\HDR.Core.Test\bin\Debug\netcoreapp2.1\HDFql.so;

Copy-Item   -Path .\temp\HDFQL_windows\hdfql-1.5.0\wrapper\csharp\HDFql.dll `
            -Destination .\test\HDR.Core.Test\bin\Debug\netcoreapp2.1\HDFql.dll;

dotnet test .\test\HDR.Core.Test\HDR.Core.Test.csproj;

Remove-Item -Path .\temp -Recurse -Force;